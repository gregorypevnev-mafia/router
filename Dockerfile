FROM nginx

# Cleaning up any existing configuration files
RUN rm -rf /etc/nginx/
RUN rm -rf /var/www/html/

# Copy entire configuration directory
COPY ./config/ /etc/nginx/

# Copy the static files
COPY ./static/ /var/www/html/

# Volume for outputting logs into
VOLUME /var/log/nginx
